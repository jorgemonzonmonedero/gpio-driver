/**
 * Author: Jorge
 * Description: the aim of this driver is to initialise the GPIO, write in its pins and, read from its pins. 
*/

/**
 * Includes section
*/
#include <linux/kernel.h>		// This file contains the essential definitions for kernel programing	
#include <linux/init.h>			// This file contains definitions and macros related to the initialising and setup of kernel code and modules
#include <linux/module.h>		// This file contains declarations, macros, and functions neccessary for handling kernel modules.
#include <linux/kdev_t.h>		// This file contains definitions and macros related to device identifiers
#include <linux/fs.h>			// This file contains declarations, structures, and functions related to the filesystem implementation and interactions within the kernel
#include <linux/cdev.h>			// This file contains declarations and structures related to the character device interface and the registration of character device within the kernel
#include <linux/device.h>		// This file contains declarations, structures, and functions related to device model framework. It provides an abstraction for device representation and managment
#include <linux/delay.h>		// This file contains functionalities related to delaying execution or introducing pauses in kernel codes
#include <linux/uaccess.h>  	// This file provides functions and macros for user-space memory access and manipulation from kernel space
#include <linux/gpio.h>    		// This file provides GPIO functionalities
#include <linux/err.h>			// This file provides funcions to detect error
#include <linux/ioctl.h>        // This file provides IOCTL functionality
#include <linux/slab.h>         // This file provides functions to handle memory

/**
 * Define section
*/
#define WR _IOW('a','a',int32_t*)
#define RD _IOR('a','b',int32_t*) 
#define k_mem_size 1024

/**
 * Global variables 
*/
dev_t dev = 0;
static struct class *dev_class;
static struct cdev gpio_f_cdev;
uint8_t *k_buff;

/**
 * Loading functions
*/
static int __init gpio_f_driver_init(void);
static void __exit gpio_f_driver_exit(void);

/**
 * Driver functions 
*/
static int gpio_f_open(struct inode *inode, struct file *file);
static int gpio_f_release(struct inode *inode, struct file *file);
static ssize_t gpio_f_read(struct file *file, char __user *buff, size_t len, loff_t *off);
static ssize_t gpio_f_write(struct file *file, const char *buff, size_t len, loff_t *off);
static long gpio_f_ioctl(struct file *file, unsigned int cmd, unsigned long arg);

/**
 * File operation structure
*/
static struct file_operations fops =
{
  .owner          = THIS_MODULE,
  .read           = gpio_f_read,
  .write          = gpio_f_write,
  .unlocked_ioctl = gpio_f_ioctl,
  .open           = gpio_f_open,
  .release        = gpio_f_release,
};

/**
 * Function definitions
*/

/**
 * 
 * 
 */
static int gpio_f_open(struct inode *inode, struct file *file){
	pr_info("Device file is opened\n");
	return 0;
}

/**
 * 
 * 
 */
static int gpio_f_release(struct inode *inode, struct file *file){
	pr_info("Device file is closed\n");
	return 0;
} 

/**
 * 
 * 
 */
static ssize_t gpio_f_read(struct file *file, char __user *buff, size_t len, loff_t *off){
	// Send value to user-space application
	if(copy_to_user(buff, k_buff,k_mem_size)){
		pr_err("Reading operation was not possible\n");
	}
	pr_info("Reading operation successfully done - GPIO_X");
    return k_mem_size;
}

/**
 * 
 * 
 */
static ssize_t gpio_f_write(struct file *file, const char *buff, size_t len, loff_t *off){
	// Get values from user-space application
	if(copy_from_user(k_buff, buff, len)){	
		pr_err("Error: Bytes do not got\n");
	}
	pr_info("Performing writing operation - GPIO_X=");
	return len;
}

/**
 * 
 * 
 */
static long gpio_f_ioctl(struct file *file, unsigned int cmd, unsigned long arg){
    int32_t val;
    switch(cmd){
    case WR:
        if(copy_from_user(&val,(int32_t*) arg,sizeof((int32_t*) arg))){
		pr_err("Copy from error\n");
        }
	gpio_unexport(val&0x0000001F);
	gpio_free(val&0x0000001F);
	gpio_request(val&0x0000001F,"GPIO_U");
	gpio_direction_output(val&0x0000001F,0);
	gpio_export(val&0x0000001F,false);
        gpio_set_value(val&0x0000001F,(val&0x00000020)>>5);
        break;
    case RD:
	gpio_unexport(arg&0x0000001F);
	gpio_free(arg&0x0000001F);
	gpio_request(arg&0x0000001F,"GPIO_U");
	gpio_export(arg&0x0000001F,false);
	gpio_get_value(arg&0x0000001F);
        if(copy_to_user((int32_t*) arg,&val,sizeof(val))){
        	pr_err("Copy to error\n");
	}
        break;
    default:
        pr_info("Default case\n");
        break;
    }
    return 0;
}

/**
 * Module functions
*/
/**
 * 
 * 
 */
static int __init gpio_f_driver_init(void){
	/*Allocating Major number*/
    if((alloc_chrdev_region(&dev, 0, 1, "gpio_f_Dev")) <0){
        pr_info("Cannot allocate major number\n");
        goto r_unreg;
    }
    pr_info("Major = %d Minor = %d \n",MAJOR(dev), MINOR(dev));
 
    /*Creating cdev structure*/
    cdev_init(&gpio_f_cdev,&fops);
 
    /*Adding character device to the system*/
    if((cdev_add(&gpio_f_cdev,dev,1)) < 0){
        pr_info("Cannot add the device to the system\n");
        goto r_del;
    }
 
    /*Creating struct class*/
    if(IS_ERR(dev_class = class_create(THIS_MODULE,"gpio_f_class"))){
        pr_info("Cannot create the struct class\n");
        goto r_class;
    }
 
    /*Creating device*/
    if(IS_ERR(device_create(dev_class,NULL,dev,NULL,"gpio_f_device"))){
        pr_info("Cannot create the device 1\n");
        goto r_device;
    }

    /*Creating physical memory*/
    if((k_buff=kmalloc(k_mem_size,GFP_KERNEL)) == 0){
        pr_info("Allocating physical memory was not possible\n");
        goto r_device;
    }

    pr_info("Device driver inserted\n");
    return 0;
 	
    r_device:
        device_destroy(dev_class,dev);
    r_class:
    	class_destroy(dev_class);
    r_del:
    	cdev_del(&gpio_f_cdev);
    r_unreg:
        unregister_chrdev_region(dev,1);
    return -1;
}

/**
 * 
 * 
 */
static void __exit gpio_f_driver_exit(void){
  	kfree(k_buff);
        device_destroy(dev_class,dev);
 	class_destroy(dev_class);
  	cdev_del(&gpio_f_cdev);
  	unregister_chrdev_region(dev, 1);
  	pr_info("Device Driver Removed\n");
}

module_init(gpio_f_driver_init);
module_exit(gpio_f_driver_exit);
MODULE_LICENSE("GPL");
