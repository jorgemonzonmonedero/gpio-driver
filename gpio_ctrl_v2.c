/**
 * Author: 
 * Description:
*/

/**
 * Includes section
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define WR _IOW('a','a',int32_t*)
#define RD _IOR('a','b',int32_t*)

int main(){
 	int fd;
 	int32_t pin,val,pin_val,val_rd;
 	printf("Initialisng user app\n");
 	fd = open("/dev/gpio_f_device",O_RDWR);
 	if (fd<0)
 	{
 		printf("Cannot open device file\n");
 		return 0;
 	}
 	while(1){
 		printf("Enter a valid GPIO pin (from 0 to 27)\n");
 		scanf("%d", &pin);
 		getchar();
 		printf("Enter 1 for write or 0 for read\n");
 		char option;
 		scanf("%c", &option);
 		getchar();
 		switch(option){
 		case '0':
 			ioctl(fd,RD,(int32_t*) &val_rd);
 			printf("Reading value %d from GPIO_%d (0x%x)\n",val_rd,pin,pin);
 			break;
 		case '1':
 			printf("Please select high(1) or low(0) level\n");
 			scanf("%d", &val);
 			pin_val = ((val&0x01)<<5)|(pin&0x01F);
 			ioctl(fd,WR,(int32_t*) &pin_val);
 			printf("Writing value %d in GPIO_%d (0x%x)\n",(pin_val&0x20)>>5,pin_val&0x1F,pin&0x1F);
			printf("Pin_val=0x%x\n",pin_val);
 			break;
 		default:
 			printf("Non-valid operation\n");
 			break;
 		}
 	}
	close(fd);
	return 0; 	
} 	

